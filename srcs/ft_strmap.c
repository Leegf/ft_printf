/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/30 18:00:51 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:24:22 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Applies the function f to each character of the string given as argument
** to create a “fresh” new string (with malloc(3)) resulting from
** the successive applications of f.
*/

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*mapped_string;
	size_t	i;
	size_t	len_of_initial_str;

	i = 0;
	if (!s || !f)
		return (NULL);
	len_of_initial_str = ft_strlen(s);
	mapped_string = ft_strnew(len_of_initial_str);
	if (mapped_string == NULL)
		return (NULL);
	while (i < len_of_initial_str)
	{
		mapped_string[i] = f(s[i]);
		i++;
	}
	return (mapped_string);
}
