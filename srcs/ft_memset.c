/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 21:26:31 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:19:16 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The memset() function writes len bytes of value c
** (converted to an unsigned char) to the string b.
** The memset() function returns its first argument.
*/

void	*ft_memset(void *b, int c, size_t len)
{
	size_t			i;
	unsigned char	tmp;
	unsigned char	*hm;

	i = 0;
	tmp = (unsigned char)c;
	hm = (unsigned char *)b;
	while (i < len)
	{
		hm[i] = tmp;
		i++;
	}
	return (&hm[0]);
}
