/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:57:39 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/20 15:51:19 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Allocates (with malloc(3)) and returns a “fresh” link.
** The variables content and content_size of the new link are
** initialized by copy of the parameters of the function.
** If the pa- rameter content is nul, the variable content is initialized
** to NULL and the variable content_size is initialized to 0 even if
** the parameter content_size isn’t. The variable next is initialized to
** NULL. If the allocation fails, the function returns NULL.
*/

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list *tmp;

	tmp = (t_list*)malloc(sizeof(t_list));
	if (tmp == NULL)
		return (NULL);
	if (content == NULL)
	{
		tmp->content = NULL;
		tmp->content_size = 0;
		tmp->next = NULL;
	}
	else
	{
		tmp->content_size = content_size;
		tmp->content = malloc(content_size);
		if (tmp->content == NULL)
		{
			free(tmp);
			return (NULL);
		}
		tmp->content = ft_memmove(tmp->content, content, content_size);
		tmp->next = NULL;
	}
	return (tmp);
}
