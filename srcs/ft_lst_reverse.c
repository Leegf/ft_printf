/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_reverse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/07 15:37:39 by lburlach          #+#    #+#             */
/*   Updated: 2017/12/06 18:06:51 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** It'll reverse list elements into backwards order
*/

void	ft_lst_reverse(t_list **alst)
{
	size_t size;
	size_t i;
	t_list *tmp;

	if (!alst || !(*alst))
		return ;
	tmp = NULL;
	size = ft_lst_size(*alst);
	i = 1;
	while (i < size + 1)
	{
		ft_lst_push_back(&tmp, ft_lst_at(*alst, size - i)->content,
				ft_lst_at(*alst, size - i)->content_size);
		i++;
	}
	*alst = tmp;
}
