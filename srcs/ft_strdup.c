/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strdup.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 21:11:58 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:24:05 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** The strdup() function allocates sufficient memory for a copy of the
** string s1, does the copy, and returns a pointer to it. The pointer may
** subsequently be used as an argument to the function free(3).
** If insufficient memory is available, NULL is returned.
** The strndup() function copies at most n characters from the string
** s1 always NUL terminating the copied string.
*/

char	*ft_strdup(const char *s1)
{
	size_t	i;
	char	*cp;
	size_t	len;

	len = ft_strlen(s1);
	i = 0;
	cp = (char *)malloc(sizeof(char) * len + 1);
	if (cp == NULL)
		return (NULL);
	while (i < len)
	{
		cp[i] = s1[i];
		i++;
	}
	cp[i] = '\0';
	return (cp);
}
