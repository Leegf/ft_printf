/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 21:34:03 by lburlach          #+#    #+#             */
/*   Updated: 2017/11/10 16:23:53 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** strcpy() functions copies the string src to dst (including the
** terminating `\0' character.)  The source and destination strings should
** not overlap, as the behavior is undefined. Returns dst.
*/

char	*ft_strcpy(char *dst, const char *src)
{
	size_t len;
	size_t i;

	i = 0;
	len = ft_strlen(src);
	while (i <= len)
	{
		dst[i] = src[i];
		i++;
	}
	return (dst);
}
