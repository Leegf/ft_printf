/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   string3.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lburlach <lburlach@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/01/23 17:08:00 by lburlach          #+#    #+#             */
/*   Updated: 2018/01/23 17:08:00 by lburlach         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

/*
** Corrects width without paying attention to precision.
*/

void	correct_width(t_flags *flags, wchar_t *st)
{
	unsigned int	size;
	int				i;

	i = 0;
	while (st[i])
	{
		size = count_those_great_bits(st[i]);
		if (size > 7 && size <= 11)
			(*flags).width -= 1;
		else if (size > 11 && size < 17)
			(*flags).width -= 2;
		else if (size >= 17)
			(*flags).width -= 3;
		i++;
	}
}

/*
** Counts how many bytes consist of bits(size)). Returns bytes' value.
*/

int		num_of_bytes(unsigned int size)
{
	if (size <= 7)
		return (1);
	else if (size <= 11)
		return (2);
	else if (size <= 16)
		return (3);
	else
		return (4);
}

/*
** Counts how many bytes are in the entire wchar string.
*/

int		count_all_bytes(wchar_t *s)
{
	int				i;
	size_t			sum;
	unsigned int	size;

	i = 0;
	sum = 0;
	while (s[i])
	{
		size = count_those_great_bits(s[i]);
		sum += num_of_bytes(size);
		i++;
	}
	return (sum);
}
