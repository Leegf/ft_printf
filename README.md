Printf implementation of my own. Supports covnersions `sSpdDioOuUxXcC`, manages `%%`, flags `#0-+` and space. Also manages field-width and precison. Flags `hh, h, l, ll, j, z` are taken into account as well.

To compile: `make`.

Other commands: `make re`, `make clean`, `make fclean`.

P.S. Printf is added to `libft` rep. 